       ID DIVISION.
       PROGRAM-ID. DATDEMO.
      *    THIS DEMONSTRATES Impact build with CICS/BMS
      *
      * region is cicsts61 on myWazi
      * Tran ='DAT0' in rpl ZDEV.IDZ.LOAD       
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *
      *    COPY DFHAID.
      *
      * My test with my pre-built map 
           COPY DATMAPM.
       PROCEDURE DIVISION.
           DISPLAY 'DATMAPM pgm v1'.
           EXEC CICS
                SEND MAP ('DATMLIS')
                     MAPSET('DATMAPM')
                     FROM(DATMLISO)
           END-EXEC.
      *
      *  this sub is a dual mod for datbatch and datdemo
      * 
      *    CALL 'DATSUB'.
           STOP RUN.
